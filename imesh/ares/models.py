#encoding:utf-8
from django.db import models
from django.utils.encoding import smart_str, smart_unicode
from django.contrib.auth.models import User, UserManager, Group

from ubigeo.models import Ubigeo

from datetime import datetime

class Usuario(User):
	direccion = models.CharField(max_length = 250, verbose_name = "Dirección", help_text = "Ingrese la dirección.", null = False, blank = True)
	telfijo = models.CharField(max_length = 15, verbose_name = "Teléfono fijo", help_text = "Ingrese el teléfono fijo.", null = False, blank = True)
	telmobil = models.CharField(max_length = 15, verbose_name = "Teléfono móbil", help_text = "Ingrese el teléfono móvil.", null = False, blank = True)	
	#objects = UserManager()
	def __unicode__(self):
		return  u'%s' % (self.username + " - " + self.last_name + " " + self.first_name)

	def get_Grupos(self):
		grupos = ""
		for x in self.groups.all():
			grupos += ", "+str(x.name)
		return u'%s' % (grupos[2:len(grupos)])

	def get_Pseudo(self):
		if self.first_name and self.last_name:
			return (self.first_name[0] + self.last_name.split(" ")[0])
		else:
			return self.first_name + " " + self.last_name

	def get_nombres(self):
		return smart_str(self.last_name + " " + self.first_name)

	def get_active_check(self):
		if self.is_active:
			return "checked"
		else:
			return ""

	class Meta:
		app_label = 'security'

class Persona(Usuario):
	# Obligatorio
	dni = models.CharField(max_length=8, verbose_name='DNI', help_text='Porfavor indique el DNI', unique=True)
	appaterno = models.CharField(max_length = 70, verbose_name = "Apellido Paterno", help_text = "Ingrese el Apellido Paterno.", null = False, blank = False)
	apmaterno = models.CharField(max_length = 70, verbose_name = "Apellido Materno", help_text = "Ingrese el Apellido Materno.", null = False, blank = False)
	nombre = models.CharField(max_length = 70, verbose_name = "Nombres", help_text = "Ingrese el Nombre.", null = False, blank = False)

	# Opcional
	nacimiento = models.DateField(verbose_name = "Fecha de Nacimiento", help_text = "Seleccione la fecha de nacimiento.", default = datetime.now(), null = False, blank = True)
	ubigeo = models.ForeignKey(Ubigeo, verbose_name = "Seleccione el distrito.", null=True, blank=True)

	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Modificación.", help_text = "Ingrese la Fecha de Modificación.", null = False, blank = False)
	#usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")
	objects = UserManager()
	def get_nombres(self):
		return smart_str(self.appaterno + " " + self.apmaterno + " " + self.nombre)

	def get_ubigeo_text(self):
		return self.ubigeo.get_resumen()

	class Meta:
		app_label = 'colmena'

class Adulto(Persona):
	# Opcional
	profesion = models.CharField(max_length=250, verbose_name='Profesión', help_text='Porfavor indique la profesión', blank=True, null = False)
	centrotrabajo = models.CharField(max_length=250, verbose_name='Centro de trabajo', help_text='Porfavor indique el centro de trabajo', blank=True, null = False)
	cargo = models.CharField(max_length=250, verbose_name='Cargo que desempeña', help_text='Porfavor indique el cargo que desempeña', blank=True, null = False)
	OPC = [["S","Soltero"], ["C", "Casado"], ["D", "Divorciado"], ["V", "Viudo"], ["M", "Menor de Edad"]]
	civil = models.CharField(max_length = 1, verbose_name = "Estado civil", help_text = "Seleccione el Estado Civil.", choices = OPC, blank = True, null = False )
	def __unicode__(self):
		return "Padre"
	class Meta:
		app_label = 'colmena'
		
class Alumno(Persona):
	padre = models.ForeignKey(Adulto, verbose_name='Padre', help_text='Porfavor indica datos del padre', null=True, related_name='Padre', blank=True)
	madre = models.ForeignKey(Adulto, verbose_name='Madre', help_text='Porfavor indica datos de la madre', null=True, related_name='Madre', blank=True)
	apoderado = models.ForeignKey(Adulto, verbose_name='Apoderado', help_text='Porfavor indica datos del apoderado', null=True, related_name='Apoderado', blank=True)
	imagen = models.ImageField(upload_to = 'alumno/ %Y/ %m/ %d', null = False, blank = True)
	def __unicode__(self):
		return "Alumno"
	class Meta:
		app_label = 'colmena'

class Ejercicio(models.Model):
	anio = models.PositiveIntegerField(max_length=4, verbose_name='Año', help_text='Porfavor indique el año', primary_key=True)
	estado = models.BooleanField(verbose_name = "Estado", help_text = "Estado", default = False, null = False, blank = False)
	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Modificación.", help_text = "Ingrese la Fecha de Modificación.", null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")	
	def __unicode__(self):
		return "Ejercicio"

	class Meta:
		app_label = 'colmena'

class Periodo(models.Model):
	nombre = models.CharField(max_length=250, verbose_name='Nombre del periodo', help_text='Porfavor ingrese el nombre del periodo')
	ejercicio = models.ForeignKey(Ejercicio, verbose_name='Año', help_text='Porfavor indique el año')
	estado = models.BooleanField(verbose_name = "Estado", help_text = "Estado", default = True, null = False, blank = False)
	inicio = models.DateField(verbose_name="Fecha de inicio", help_text = "Ingrese la Fecha de inicio.", null = False, blank = False)
	fin = models.DateField(verbose_name="Fecha de finalización", help_text = "Ingrese la Fecha de finalización.", null = False, blank = False)
	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Modificación.", help_text = "Ingrese la Fecha de Modificación.", null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")
	def __unicode__(self):
		return "Periodo"

	class Meta:
		app_label = 'colmena'

class Docente(Usuario):
	# Opcional
	profesion = models.CharField(max_length=250, verbose_name='Profesión', help_text='Porfavor indique la profesión', blank=True, null = False)

	def __unicode__(self):
		return "Docente"

	class Meta:
		app_label = 'colmena'

class Grado(models.Model):
	O = [["I","Inicial"], ["P", "Primaria"], ["S", "Secundaria"]]
	nivel = models.CharField(max_length=2, verbose_name='Nivel', help_text='Porfavor indique el nivel', choices = O, blank = False, null = False)
	nombre = models.CharField(max_length=250, verbose_name='Nombre', help_text='Porfavor ingrese el nombre del grado', blank = False, null = False)
	estado = models.BooleanField(verbose_name = "Estado", help_text = "Estado", default = True, null = False, blank = False)
	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Modificación.", help_text = "Ingrese la Fecha de Modificación.", null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")
	def __unicode__(self):
		return "Grado"

	class Meta:
		app_label = 'colmena'

class Seccion(models.Model):
	periodo = models.ForeignKey(Periodo, verbose_name='Periodo', help_text='Porfavor indique el periodo')
	grado = models.ForeignKey(Grado, verbose_name = "Grado")
	nombre = models.CharField(max_length=50, verbose_name='Nombre', help_text='Porfavor ingrese el nombre', blank = False, null = False)
	aforo = models.PositiveIntegerField(max_length = 2, verbose_name = "Aforo", help_text='Porfavor ingrese el aforo')
	tutor = models.ForeignKey(Docente, related_name="tutor", verbose_name = "Seleccione un Docente")
	estado = models.BooleanField(verbose_name = "Estado", help_text = "Estado", default = True, null = False, blank = False)
	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Modificación.", help_text = "Ingrese la Fecha de Modificación.", null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")	
	def __unicode__(self):
		return smart_str(self.nombre) + " " + smart_str(self.grado.nombre)

	class Meta:
		app_label = 'colmena'

class Matricula(models.Model):
	alumno = models.ForeignKey(Alumno, verbose_name='Alumno a matricular', help_text='Porfavor indique el alumno a matricular')
	seccion = models.ForeignKey(Seccion, verbose_name='Sección en la que se va a matricular', help_text='Porfavor indique la sección en la que se va a matricular')
	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Modificación.", help_text = "Ingrese la Fecha de Modificación.", null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario", related_name='matricula_usuario_creacion')
	def __unicode__(self):
		return smart_str(self.alumno.dni) + " - " + smart_str(self.seccion.nombre) + " - " + smart_str(self.seccion.grado.nombre)
	
	class Meta:
		app_label = 'colmena'