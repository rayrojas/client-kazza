from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
	url('^$', 'ares.views.index'),
	url('^api/ares/rosadelima/index.js$', 'ares.views.js_rosadelima_index'),
)
