#encoding:utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User, Group
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required

from sislog import logear
from ares.models import Alumno

import datetime
import json
import traceback
import sys

def getAplicacion():
	return {
		'site_root' : 'http://127.0.0.1:8000/',
		'root' : '/courier/',
		'app' : {'nombre' : 'Sistema de Información Rosa de Lima'}
	}


@login_required
def index(request):
	usuario = request.user
	try:
		usuario = Alumno.objects.get(id = usuario.id)
	except Exception, e:
		messages.error(request, "No se encontró un usuario con código: " + str(usuario.id))
		error = traceback.extract_stack()[-1]
		logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
		return HttpResponseRedirect("/404/")
	context = {
		'aplicacion' : getAplicacion(),
		#'items' : getMenuFirst(usuario.groups.all()),
		#'menu' :  getMenuSecond(usuario.groups.all()),
		#'itres' : getThirdSecond(usuario.groups.all()),
		'usuario' : usuario,
	}
	return render_to_response('index.html', context, context_instance = RequestContext(request))

@login_required
def js_rosadelima_index(request):
	doReturn = 'var generals = { user: "' + str(request.user.id) + '", dni: "' + request.user.username + '" };';
	return HttpResponse(doReturn, mimetype='application/javascript')