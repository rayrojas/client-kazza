from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
admin.autodiscover()
urlpatterns = patterns('',
	url('^login/$', 'imesh.views.login'),
	url('^accounts/login/', 'imesh.views.login'),
	url('^logout/$', 'imesh.views.onlogout'),
	url(r'^api/', include('api.urls')),
	url(r'^', include('ares.urls')),
)

