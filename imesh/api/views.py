from django.utils import simplejson
from django.core.serializers import json
from tastypie.resources import ModelResource, fields
from tastypie.authentication import SessionAuthentication
from tastypie.serializers import Serializer
from ares.models import Alumno, Matricula

class BKReadAlumno(ModelResource):	
	class Meta:
		queryset = Alumno.objects.all()
		resource_name = "read/alumno"
		filtering = {
			'dni' : ['exact',],
		}
		limit = 50
		authentication = SessionAuthentication()

	def dehydrate(self, bundle):
			bundle.data['nombrestext'] = bundle.obj.get_nombres()
			bundle.data['ubigeotext'] = bundle.obj.ubigeo.get_codigo()
			bundle.data['ubigeo_resumen_text'] = bundle.obj.get_ubigeo_text()
			bundle.data['padre'] = bundle.obj.padre
			if bundle.data['padre'] is None:
				bundle.data['padre'] = "0"
			else:
				bundle.data['padre'] = str(bundle.data['padre'].dni)
			bundle.data['madre'] = bundle.obj.madre
			if bundle.data['madre'] is None:
				bundle.data['madre'] = "0"
			else:
				bundle.data['madre'] = str(bundle.data['madre'].dni)
			bundle.data['apoderado'] = bundle.obj.apoderado
			if bundle.data['apoderado'] is None:
				bundle.data['apoderado'] = "0"
			else:
				bundle.data['apoderado'] = str(bundle.data['apoderado'].dni)
			return bundle

class BKReadMatricula(ModelResource):
	dni = fields.CharField(attribute = 'alumno__dni')
	ejercicio = fields.CharField(attribute = 'seccion__periodo__ejercicio__anio')
	class Meta:
		queryset = Matricula.objects.all()
		resource_name = "read/matricula"
		filtering = {
			'dni' : ['exact',],
		}
		limit = 50
		authentication = SessionAuthentication()		