from django.conf.urls.defaults import patterns, url, include
from api.views import BKReadAlumno, BKReadMatricula

urlpatterns = patterns('',
	url('^core/', include(BKReadAlumno().urls)),
	url('^core/', include(BKReadMatricula().urls)),
)
