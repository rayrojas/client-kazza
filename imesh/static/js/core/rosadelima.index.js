var Router = Backbone.Router.extend({
	routes: {
		"": "inicio",
		"horario": "horario",
	},
	initialize: function () {},
	inicio: function () {
		$.get("/api/core/read/alumno/?format=json&dni=" + generals['dni'])
		.success(function(request){
			$.get("/static/js/views/academico/alumno.html")
			.success(function(template){
				if ( request['meta']['total_count'] > 0 ) {
					$("#rosadelima-viewport").html(Mustache.to_html(template, request['objects'][0]))
				}
			});
		})
		.error(function(){

		});
	},
	horario: function() {
		$.get("/api/core/read/matricula/?format=json&dni=" + generals['dni'])
		.success(function(request){
			$.get("/static/js/views/academico/horario.html")
			.success(function(template){
				if ( request['meta']['total_count'] > 0 ) {
					$("#rosadelima-viewport").html(Mustache.to_html(template, request))
				}
			});
		})
		.error(function(){

		});
	}
});
app = new Router();
$(document).on("ready", function(){
	if (!bowser.firefox) {
		
	}
	$('div#da-content #da-main-nav > ul > li').on('click', ' > a, > span', function(e) {
		if( $(this).next('ul').length ) {
			$(this).next('ul').first().slideToggle('normal', function() {
				$(this).toggleClass('closed');
			});
			e.preventDefault();
		}
	});

	Backbone.history.start();
});