$(document).ready(function(e) {
	$( '.da-home-form' ).validate({
		rules: {
			'username': {
				required: true
			}, 
			'password': {
				required: true
			}
		}
	});
	$.fn.placeholder && $('[placeholder]').placeholder();
});