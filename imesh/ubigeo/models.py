#encoding:utf-8
from django.db import models
from django.utils.encoding import smart_str

class Ubigeo(models.Model):
	id = models.CharField(primary_key=True, max_length = 9, verbose_name = "Id", help_text = "Ingrese el Id.", null = False, blank = False)	
	departamento = models.CharField(max_length=3, verbose_name = "Código departamento", help_text = "Ingrese un código del departamento.", null = False, blank = False)
	provincia = models.CharField(max_length=3, verbose_name = "Código provincia", help_text = "Ingrese un código de la provincia.", null = False, blank = False)
	distrito = models.CharField(max_length=3, verbose_name = "Código ditrito", help_text = "Ingrese un código del distrito.", null = False, blank = False)
	nombre = models.CharField(max_length = 250, verbose_name = "Nombre", help_text = "Ingrese el nombre.", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (smart_str(self.nombre))

	def get_codigo(self):
		return str(self.id[0:2] + "," + self.id[0:4] + "," + self.id[0:6])

	def get_resumen(self):
		doReturn = ''
		for x in self.get_codigo().split(','):
			doReturn += Ubigeo.objects.get(id = x).nombre + " / "
		return doReturn[:len(doReturn)-2]