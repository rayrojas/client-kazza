#encoding:utf-8
from django.utils import simplejson

from django.http import HttpResponse

from ubigeo.models import Ubigeo


def bk_departamento(request, codigo):
	context = {'status' : 'true', 'provincias' : []}
	for x in Ubigeo.objects.filter(id__startswith = codigo, distrito = "0").exclude(provincia = "0"):
		context['provincias'].append({
			'id' : str(x.id), 'nombre' : x.nombre
		})
	return HttpResponse(simplejson.dumps(context), mimetype='application/json')
	
def bk_provincia(request, codigo, provincia):
	context = {'status' : 'true', 'distritos' : []}
	for x in Ubigeo.objects.filter(id__startswith = provincia).exclude(distrito = "0"):
		context['distritos'].append({
			'id' : str(x.id), 'nombre' : x.nombre
		})
	return HttpResponse(simplejson.dumps(context), mimetype='application/json')
