from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
	url('^bk/departamento/(?P<codigo>\d{1,})/$', 'ubigeo.views.bk_departamento'),
	url('^bk/provincia/(?P<codigo>\d{1,})/(?P<provincia>\d{1,})/$', 'ubigeo.views.bk_provincia'),
)
