# -*- coding: utf-8 -*-
from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label = "DNI", max_length = 30)
	password = forms.CharField(label = "Contaseña", widget = forms.PasswordInput)
