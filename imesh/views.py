#encoding:utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.utils import simplejson
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from forms import LoginForm


def getAplicacion():
	return {
		'site_root' : 'http://127.0.0.1:8000/',
		'root' : '/courier/',
		'app' : {'nombre' : 'Sistema de Información Rosa de Lima'}
	}

def hasPermission(u, patron):
	for uu in u:
		if uu.id == patron:
			return True
	return False

def isLogin(u):
	if u is not None and u.is_active:
		return True
	else:
		return False

def nofound(request):
	return render_to_response('nofound.html', {}, context_instance = RequestContext(request))

def login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			passwd = form.cleaned_data['password']
			user = authenticate(username = username, password = passwd)
			if user is not None and not user.groups.all():
				if user.is_active:
					auth_login(request, user)
					return HttpResponseRedirect('/')
				else:
					context = {
						'formlogin' : form,
					}
			else:
				context = {
					'formlogin' : form,
				}
		else:
			form = LoginForm()
			context = {
				'formlogin' : form,
			}
	else:
		form = LoginForm()
		context = {
			'formlogin' : form,
		}
					
	return render_to_response('login.html', context, context_instance = RequestContext(request))
	
def onlogout(request):
	logout(request)
	return HttpResponseRedirect('/')


@login_required
def header_tools(request, client):
	usuario = request.user
	if client == 'alumno':
		if (hasPermission(usuario.groups.all(), 1)):
			buttons = [
				{
					'title' : 'Datos',
					'title_text' : 'Datos',
					'icon' : '',
					'url' : '/academico/alumno/datos/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Editar',
					'title_text' : 'Editar',
					'icon' : 'icon-pencil',
					'url' : '/academico/alumno/editar/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Matricula',
					'title_text' : 'Matricula',
					'icon' : 'icon-cog ',
					'url' : '/academico/alumno/grado/seccion/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
			]
		elif (hasPermission(usuario.groups.all(), 2)):
			buttons = [
				{
					'title' : 'Datos',
					'title_text' : 'Datos',
					'icon' : '',
					'url' : '/academico/alumno/datos/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Editar',
					'title_text' : 'Editar',
					'icon' : 'icon-pencil',
					'url' : '/academico/alumno/editar/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Matricula',
					'title_text' : 'Matricula',
					'icon' : 'icon-cog ',
					'url' : '/academico/alumno/grado/seccion/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
			]
		elif (hasPermission(usuario.groups.all(), 3)):
			buttons = [
				{
					'title' : 'Datos',
					'title_text' : 'Datos',
					'icon' : '',
					'url' : '/academico/alumno/datos/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Editar',
					'title_text' : 'Editar',
					'icon' : 'icon-pencil',
					'url' : '/academico/alumno/editar/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Matricula',
					'title_text' : 'Matricula',
					'icon' : 'icon-cog ',
					'url' : '/academico/alumno/grado/seccion/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
			]
		elif (hasPermission(usuario.groups.all(), 4)):
			buttons = [
				{
					'title' : 'Datos',
					'title_text' : 'Datos',
					'icon' : '',
					'url' : '/academico/alumno/datos/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Editar',
					'title_text' : 'Editar',
					'icon' : 'icon-pencil',
					'url' : '/academico/alumno/editar/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
				{
					'title' : 'Matricula',
					'title_text' : 'Matricula',
					'icon' : 'icon-cog ',
					'url' : '/academico/alumno/grado/seccion/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				},
			]
	elif client == "docente":
		if (hasPermission(usuario.groups.all(), 7)):
			buttons = [
				{
					'title' : 'Datos',
					'title_text' : 'Datos',
					'icon' : '',
					'url' : '/academico/alumno/datos/{{ template-id }}/',
					'class' : 'button plain blue spacing'
				}
			]
	else:
		return HttpResponse(status=400)
	return HttpResponse(simplejson.dumps({"status" : 'true', 'buttons' : buttons}), mimetype='application/json')